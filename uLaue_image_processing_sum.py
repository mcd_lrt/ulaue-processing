#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import fabio
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.ndimage import gaussian_filter
from scipy.ndimage import median_filter
import sys
import os
from IPython.display import Image


# In[ ]:


#cd S:/300-Projets_Transverses/300.56-Solflex


# In[1]:


def uLaue_filter(img_data,pks,bckgrd,thd=1000,L=30):
    #Funbction that takes data from .tif diffraction pattern, removes the background and data around a LxL square around the position of the peak list
    # input : .tif diffraction pattern, .dat peak list to create a mask and a background .tif file to substract to the diffraction pattern threshold and halfsize of box
    # ouput : uint16 img data 
    img_data = img_data.astype(float)
    bgrd = fabio.open(bckgrd)
    bgrd = bgrd.data.astype(float)
    #medl = np.median(od)
    #fd = od 
    #fd[fd>thd] = medl
    #df2 = img.data-bgrd2+100
    #bg_mf = median_filter(img_data-bgrd+100,size=15)
    bg = img_data-bgrd+100
    medl = np.median(bg)
    bg[bg>thd]= 0.9*medl
    f = open(pks)
    f = f.read()
    peak_data =f.split('#')[0].split('\n')[1:-1] 
    peak_num = np.zeros([len(peak_data),11])
    for iii in np.arange(len(peak_data)) :
        tmp = peak_data[iii].split('   ')
        for jjj in np.arange(11):
            peak_num[iii,jjj] = float(tmp[jjj])
    peak_pos = np.round(peak_num[:,0:2])
    mask = np.zeros_like(bg)
    for iii in np.arange(len(peak_pos)):
        mask[int(peak_pos[iii,1])-L : int(peak_pos[iii,1])+L,int(peak_pos[iii,0])-L:int(peak_pos[iii,0])+L] = 1
    #bg_mf_masked = bg_mf*mask
    bg_masked = bg*mask
    print('Filtered')
    return bg_masked.astype(np.uint16)


# In[ ]:


def filter_fileserie(prefix,s_index,imgnb,pks,bckgrd,thd=1000,L=30):
    #Function that removes backgroungd and data around peaklist for fileseries, takes number of images and prefix
    for iii in np.arange(s_index,s_index+imgnb):
        img = fabio.open(prefix+'{:0>4d}.tif'.format(iii))
        img_data = img.data
        print('{:0>4d}'.format(iii))
        d_filtered = uLaue_filter(img_data,pks,bckgrd,thd,L)
        img.data = d_filtered
        img.save('filtered/'+prefix+'{:0>4d}.tif'.format(iii))


# In[ ]:


def bgrd_sum(prefix,starting_index,imgnumber):
    #Create background and  
    img_all = np.zeros([121,2018,2016])
    s = np.zeros(((2018, 2016)))
    
    for iii in np.arange(starting_index,starting_index+imgnumber):
        bgrd = fabio.open(prefix +'{:0>4d}.tif'.format(iii))
        d = bgrd.data
        #med = np.median(d)  # median value of original image
        #d[d>thd] = med*0.9 # set hot pixels to 90 % of the median value
        d = d.astype(float)
        s += d/121
        img_all[iii-starting_index] = bgrd.data.astype(float)
        print('{:0>4d}'.format(iii))
    f = median_filter(s,size=15) # taille du filtre médian pas trop élevée sinon on garde la tache centrale dues à la ^platine
    os.makedirs('filtered')
    bgrd.data = f.astype(np.uint16) 
    bgrd.save('filtered/background_0002.tif')
    bgrd.data = np.amax(img_all,axis = 0).astype(np.uint16)
    bgrd.save('filtered/sum_0001.tif')
    bgrd.data = (np.amax(img_all,axis = 0)-f+1000).astype(np.uint16)
    bgrd.save('filtered/sum_bgrd_0001.tif')


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point1


# In[ ]:


bgrd_sum('1bulge_',1,121)


# In[ ]:


cd ..


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point2


# In[ ]:


bgrd_sum('2bulge_',1,121)


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point3


# In[ ]:


bgrd_sum('3bulge_',126,121)


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point4


# In[ ]:


bgrd_sum('4bulge_',252,121)


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point5


# In[ ]:


bgrd_sum('5bulge_',378,121)


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point6


# In[ ]:


bgrd_sum('6bulge_',518,121)


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point7    


# In[ ]:


bgrd_sum('7bulge_',647,121)


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point8    


# In[ ]:


bgrd_sum('8bulge_',774,121)


# In[ ]:


cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data\Bulge\point8\filtered    


# In[ ]:


bgrd_sum('8Bulge_bgm_',774,121)


# In[ ]:


L =  np.array([30,30,30,50,75,70,80,80])
s_in = nnp.array([1,1,126,252,378,518,647,774])
cd S:\300-Projets_Transverses\300.56-Solflex\20180829-MA4186-udiff-bulge\uLaue_data


# In[ ]:


La=  np.array([30,30,30,50,75,70,80,80])
s_in = np.array([1,1,126,252,378,518,647,774])
for iii in np.arange(2,8):
    print('point{:d}'.format(iii+1))
    data_folder = "Bulge\point{:d}".format(iii+1)
    os.chdir(data_folder)
    for jjj in np.arange(s_in[iii],s_in[iii]+121):
        img = fabio.open('{:d}bulge_{:0>4d}.tif'.format(iii+1,jjj))
        img_data = img.data
        print('Processing {:d}bulge_{:0>4d}.tif'.format(iii+1,jjj))
        d_filtered = uLaue_filter(img_data,'filtered\sum_bgrd_0001_LT_0.dat','filtered/background_0002.tif',L=La[iii])
        img.data = d_filtered
        img.save('filtered/{:d}Bulge_bgm_{:0>4d}.tif'.format(iii+1,jjj))
    os.chdir('..\..')

